const std = @import("std");

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    const wepoll_dep = b.dependency("wepoll", .{});

    const lib = b.addStaticLibrary(.{
        .name = "wepoll",
        .target = target,
        .optimize = optimize,
    });
    lib.root_module.link_libc = true;
    lib.addCSourceFiles(.{
        .dependency = wepoll_dep,
        .files = &wepoll_src_files,
        .flags = &.{},
    });
    inline for (wepoll_inc_dirs) |inc_dir| {
        lib.addIncludePath(wepoll_dep.path(inc_dir));
    }
    lib.linkSystemLibrary("ws2_32");
    for (wepoll_inc_dirs) |inc_dir| {
        if (std.mem.eql(u8, inc_dir, "src")) {
            continue;
        }
        const path = wepoll_dep.path(inc_dir).getPath(b);
        lib.installHeadersDirectory(path, "");
    }
    b.installArtifact(lib);
}

const wepoll_src_files = [_][]const u8{
    "src/afd.c",
    "src/api.c",
    "src/error.c",
    "src/init.c",
    "src/nt.c",
    "src/poll-group.c",
    "src/port.c",
    "src/queue.c",
    "src/reflock.c",
    "src/sock.c",
    "src/thread-safe-tree.c",
    "src/tree.c",
    "src/ws.c",
};

const wepoll_inc_dirs = [_][]const u8{
    "src",
    "include",
    "config/external/static",
    "config/internal/default",
};
